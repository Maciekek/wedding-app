import React from "react";
import moment from "moment";
import './CountDown.css';
import {Container, Row, Col} from 'reactstrap';

export const CountDown = (props) => {
    let daysLeft = 0;
    const getDaysLeft = () => {
        daysLeft = moment(props.finishDate, 'DD-MM-YYYY HH:mm:ss').diff(moment(), "days");
        return daysLeft;
    };

    const getHoursLeft = () => {
        const hoursLeft = moment(props.finishDate, 'DD-MM-YYYY HH:mm:ss').diff(moment(), "hours");

        return hoursLeft - (daysLeft * 24);
    };

    return (
        <Container className='component'>

            <Row>
                <Col className='title'>Pozostało</Col>
            </Row>
            <Row>
                <Col className='days-left-amount'>{getDaysLeft()}</Col>
                <Col className='hours-left-amount'>{getHoursLeft()}</Col>

            </Row>
            <Row>
                <Col className='separator'>---------------</Col>
                <Col className='separator'>---------------</Col>
            </Row>
            <Row>
                <Col className='days-left'>Dni</Col>
                <Col className='hours-left'>Godzin</Col>
            </Row>
        </Container>
    )
}


// {/*<div >*/}
// {/*<div >Pozostało: </div>*/}
// {/*/!*<img src={gif} alt=""/>*!/*/}
// {/*</div>*/}
