import React from "react";
import './presents.css';
import {Container, Row, Col} from 'reactstrap';

export const Presents = (props) => {

    return (
        <Container className='presents-component'>

            <div>
                <Row>
                  <Col>
                    <p>Na wesele przybywajcie o prezenty się nie martwcie,</p>
                    <p>By nie składać ich na stercie niechaj zmieszczą się w kopercie.</p>
                  </Col>

                </Row>
              <Row>
                <Col className={"heart"} >[|]</Col>
              </Row>
              <Row>
                <Col>
                  <br/>
                  <p>Drodzy Goście, zamiast kwiatów proponujemy złożyć datek do specjalnej puszki,
                  </p>
                  <p>którą przekażemy Gdańskiej Fundacji Korczaka. <a href="http://www.korczak.gdansk.pl/jak-nam-pomoc/pomoc-materialna-i-finansowa">Strona fundacji</a></p>
                </Col>
              </Row>
                {/*<Row>*/}
                    {/*<Col className='date-component__separator'>*/}
                        {/*-------------------------------------------*/}
                    {/*</Col>*/}
                {/*</Row>*/}
            </div>
        </Container>
    )
}
