import React from "react";
import moment from "moment";
import './date.css';
import {Container, Row, Col} from 'reactstrap';

export const Date = (props) => {
    const getDate = () => {
        return moment(props.finishDate, 'DD.MM.YYYY').format('DD.MM.YYYY');
    };
    const getHour = () => {
        return moment(props.finishDate, 'DD.MM.YYYY HH:mm').format('HH:mm');
    };

    return (
        <Container className='date-component'>
            <div>
                <Row>
                    <Col className='date-component__title'>Data</Col>
                </Row>
                <Row>
                    <Col className='date-component__date'>{getDate()}</Col>
                </Row>

                <Row className='date-component__separator'>
                    <Col>-------------------------------------------</Col>
                </Row>
            </div>
            <div>
                <Row>
                    <Col className='date-component__title'>Godzina</Col>
                </Row>
                <Row>
                    <Col className='date-component__date'>{getHour()}</Col>
                </Row>
                <Row>
                    <Col className='date-component__separator'>
                        -------------------------------------------
                    </Col>
                </Row>
            </div>
        </Container>
    )
}
