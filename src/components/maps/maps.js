import React from "react";
import {Container, Row, Col} from 'reactstrap';
import './maps.css';

class Maps extends React.Component {

  render() {


    return (
      <div className={"maps-wrapper"}>
        <Container fluid>
          <Row>
            <Col xl='6' className=''>

              <div className="maps-wrapper__title">Miejsce ślubu</div>
              <div className={"maps-wrapper__address"}>
                <div>Parafia rzymskokatolicka pw. Matki Bożej Nieustającej Pomocy</div>
                <div>Chopina 3, 83-000 Pruszcz Gdański</div>
              </div>
              <iframe className={'mapa1'}
                      src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJIwWjOVNw_UYR6pRnZFmM0lM&key=AIzaSyACHdMtuy5BSBtcg7Y1NiS9SIgb2KmwlSI&maptype=satellite"
                      allowFullScreen/>

            </Col>

            <Col xl='6' className=''>
              <div className="maps-wrapper__title">Miejsce wesela</div>
              <div className={"maps-wrapper__address"}>
                <div>Sala weselna "Ollimp"</div>
                <div>Ogrodowa 38B, 83-000 Juszkowo</div>
              </div>

              <iframe className={'mapa1'}
                      src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJTQcqfeNw_UYRdIC2oyA5XL4&key=AIzaSyACHdMtuy5BSBtcg7Y1NiS9SIgb2KmwlSI&maptype=satellite"
                      allowFullScreen/>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Maps;
