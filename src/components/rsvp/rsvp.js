import React from "react";
import './rsvp.css';
import {Container, Row, Col, Label, Input, Form, FormGroup, Button} from 'reactstrap';
import {addToDo} from '../../services/rsvp.service'

class Rsvp extends React.Component {
  constructor() {
    super();
    this.state = this.getInitialState();
  };

  componentDidMount = () => {
    try{
      const isSent = JSON.parse(window.localStorage.getItem('sent'));

      if(isSent) {
        this.setState({
          formSubmitted: true
        })
      }
    } catch (e) {
      console.log(e)
    }
  };

  getInitialState = () => {
    return {
      guestForm: {
        name: '',
        surname: '',
        confirmation: 'true',
        additionalInfo: '',
        date: ''
      },
      formSubmitted: false,
      error: false,
      buttonDisabled: true
    }
  };

  formSubmitted = () => {
    let initState = this.getInitialState();
    initState.formSubmitted = true;

    return initState;
  };

  onSubmit = (event) => {
    event.preventDefault();

    if (!this.state.guestForm.name || !this.state.guestForm.surname) {
      this.setState({error: true});
      return;
    }

    addToDo(this.state.guestForm).then(() => {
      console.log('good');
      if (this.state.guestForm.confirmation === 'true') {
        alert("Dziękujemy! Do zobaczenia niedługo!");
        this.setState(this.formSubmitted());
        window.localStorage.setItem("sent", "true");
      } else {
        alert("Dziękujemy za informacje i pozdrawiamy.");
      }
    })
  };

  handleChange = (event) => {
    const guestForm = this.state.guestForm;
    guestForm[event.target.name] = event.target.value;

    this.setState({
      guestForm
    });

    if (this.getInitialState().guestForm.name !== this.state.guestForm.name &&
      this.getInitialState().guestForm.surname !== this.state.guestForm.surname) {
      this.setState({
        buttonDisabled: false
      })
    }

    if (this.getInitialState().guestForm.name === this.state.guestForm.name &&
      this.getInitialState().guestForm.surname === this.state.guestForm.surname) {
      this.setState({
        buttonDisabled: true
      })
    }
  };

  render() {
    const form = (
      <Container>
        <Row>
          <Col sm="6" md={{size: 6, offset: 6}}>
            <div className='rsvp__title'>
              RSVP
            </div>
            <div className='rsvp__description'>
              Bardzo nam zależy na Waszej obecności podczas tak ważnego dla nas dnia.
              Możecie dać nam znać o swojej obecności wypełniając poniższy formularz
              <div className='rsvp__description--separator'></div>
            </div>

            <Form onSubmit={this.onSubmit} className='rsvp__form'>
              <FormGroup>
                <Label for="name">Imię</Label>
                <Input name="name"
                       id="name"
                       placeholder="Imie jednej osoby z zaproszenia"
                       value={this.state.guestForm.name}
                       onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="surname">Nazwisko</Label>
                <Input name="surname" id="surname"
                       placeholder="Nazwisko jednej osoby z zaproszenia"
                       value={this.state.guestForm.surname}
                       onChange={this.handleChange}
                />
              </FormGroup>

              <FormGroup>
                <Label>Czy potwierdzasz przybycie:</Label>

                <div className='rsvp__form--confirmation'>
                  <Col xs='5'>
                    <label>
                      <Input
                        type="radio"
                        name="confirmation"
                        value='true'
                        checked={this.state.guestForm.confirmation === 'true'}
                        onChange={this.handleChange}
                      />
                      Jasne! Będziemy!
                    </label>
                  </Col>
                  <Col xs='5'>
                    <label>
                      <Input
                        type="radio"
                        value='false'
                        checked={this.state.guestForm.confirmation === 'false'}
                        onChange={this.handleChange}
                        name="confirmation"/>
                      Niestety, nie będziemy
                    </label>
                  </Col>
                </div>
              </FormGroup>

              <FormGroup>
                <Label for="additionalInfo">Dodatkowe informacje</Label>
                <Input type='textarea' name="additionalInfo" id="additionalInfo"
                       placeholder="Czy coś chcesz nam przekazać?"
                       value={this.state.guestForm.additionalInfo}
                       onChange={this.handleChange}/>
              </FormGroup>

              {this.state.error
                ? <div className={"error"}>Prosimy o wypełnienie wszystkich pól (a przynajmniej imie i nazwisko;))</div>
                : ""
              }

              <Button color={this.state.buttonDisabled ? "secondary" : "primary"} type="submit" disabled={this.state.buttonDisabled}>Wyślij</Button>

            </Form>
          </Col>
        </Row>
      </Container>
    );

    return (
      <div className='rsvp-component'>
        {!this.state.formSubmitted
          ? form
          : <Col sm="6" md={{size: 6, offset: 6}}>
            <div className='rsvp__description'>
              Dziękujemy za informacje. Do zobaczenia niedługo :)
              <div className='rsvp__description--separator'></div>
            </div>
          </Col>
        }
      </div>
    )
  }
}

export default Rsvp;
