import React from "react";
import './side-image.css';
import {Row, Col} from 'reactstrap';
import {Jumbotron, Container} from 'reactstrap';
import ScrollableAnchor from "react-scrollable-anchor";


export const SideImage = (props) => {


    let left = (
        <Row>
            <Col xl='6' className='image' style={{backgroundImage: `url(${props.image})`}}></Col>
            <Col xl='6' className='side-component'> {props.children} </Col>
        </Row>
    );

    let right = (
        <Row>
            <Col xl='6' className='side-component'> {props.children} </Col>
            <Col xl='6' className='image' style={{backgroundImage: `url(${props.image})`}}></Col>
        </Row>
    );

    return (
            <div>
                <Jumbotron fluid>
                    <Container fluid>

                        {props.imagePosition === 'left' ? left : right}


                        {/*{props.imagePosition === 'left' ?*/}
                        {/*'Left'*/}
                        {/*:'right'*/}
                        {/*}*/}


                        {/*{props.imagePosition === 'right' ?*/}
                        {/*<Col md='6' className='image' style={{ backgroundImage: `url(${props.image})` }}></Col>*/}
                        {/*: null*/}
                        {/*}*/}
                        {/*<Col md='6' className='side-component'> {props.children} </Col>*/}

                    </Container>
                </Jumbotron>
            </div>
    )
}
//
// <div className='side-image-component'>
//
//     <div className='clearfix'>
//         <Col md='6' className='image' style={{ backgroundImage: `url(${props.image})` }}></Col>
//         <Col md='6' className='side-component'> {props.children} </Col>
//     </div>
//
// </div>