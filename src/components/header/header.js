import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink} from 'reactstrap';
import { goToAnchor } from 'react-scrollable-anchor'

export default class Header extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    goto(sectionName) {
        goToAnchor(sectionName, false)
    }

    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">
                        <div className={'header-logo'}>
                            <span className={"footer-logo"}>Anna|Maciej </span>
                            {/*<FontAwesomeIcon icon={faHeart} color='red' size='lg' />*/}
                            {/*<span> Maciek</span>*/}
                        </div>
                    </NavbarBrand>
                    {/*<Collapse isOpen={this.state.isOpen} navbar>*/}
                        {/*<Nav className="ml-auto" navbar>*/}
                            {/*<NavItem>*/}
                                {/*<NavLink onClick={() => {this.goto('finish')}}>Data Ślubu</NavLink>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<NavLink onClick={() => {this.goto('count')}}>Pozostało</NavLink>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<NavLink onClick={() => {this.goto('gifts')}}>Prezenty</NavLink>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<NavLink onClick={() => {this.goto('rsvp')}}>RSVP</NavLink>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<NavLink onClick={() => {this.goto('maps')}}>Mapy</NavLink>*/}
                            {/*</NavItem>*/}
                        {/*</Nav>*/}
                    {/*</Collapse>*/}
                </Navbar>
            </div>
        );
    }
}