import React from "react";
import {Jumbotron, Container} from 'reactstrap';
import './footer.css';

export const Footer = () => {


  // let left = (
  //   <Row>
  //     <Col xl='6' className='image' style={{backgroundImage: `url(${props.image})`}}></Col>
  //     <Col xl='6' className='side-component'> {props.children} </Col>
  //   </Row>
  // );
  //
  // let right = (
  //   <Row>
  //     <Col xl='6' className='side-component'> {props.children} </Col>
  //     <Col xl='6' className='image' style={{backgroundImage: `url(${props.image})`}}></Col>
  //   </Row>
  // );
  const opts = {
    height: '390',
    width: '640',
    playerVars: { // https://developers.google.com/youtube/player_parameters
      autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3
    }
  };

  return (
    <div>
      <Jumbotron fluid className={'footer-wrapper'}>

        <Container fluid>
            <div className={'heart footer-logo'}>Anna|Maciej</div>
        </Container>
      </Jumbotron>
    </div>
  )
};