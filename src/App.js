import React, {Component} from 'react';
import './App.css';
import Header from "./components/header/header";
import grupowe from './assets/grupowe.jpg';
import wesele from './assets/wesele.jpg';
import slub from './assets/slub.jpg';
import przygotowania from './assets/przygotowania.jpg';
import movie from './assets/movie.jpg';
import plener from './assets/plener.jpg';
import my from './assets/my.jpg';
import {Footer} from "./components/footer/footer";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header></Header>

              <img src={my} style={{'width': '100%', "margin-bottom": "40px"}} alt=""/>


              <div className={'thanks'}>Dziękujemy Wam za przybycie i za wspólną zabawę.</div>
              <div className={'thanks'}>Dzięki Wam udało się zebrać <strong>1000 zł</strong>, które przekazaliśmy
                   <a href="https://korczak.gdansk.pl/"> Fundacji Korczaka</a></div>


                <div className={'thanks'} style={{"margin-top":" 40px"}}> <strong>Zapraszamy do zdjęć:</strong></div>

              <div className={'gallery'}>
                <a target="_blank"  href={'https://photos.app.goo.gl/rQsDnKwpE8BXBrcQ8'} className={'image '}>
                  <img src={przygotowania} alt=""/>
                  <span className={'title'}>Przygotowania</span>
                </a>
                <a target="_blank" href={'https://photos.app.goo.gl/gFRYRZSJX2PaPU6q7'} className={'image '}>
                  <img src={slub} alt=""/>
                  <span className={'title'}>Ślub</span>
                </a>
                <a target="_blank" href={'https://photos.app.goo.gl/NVvk2TdKAwfSQ6yV9'} className={'image '}>
                  <img src={wesele} alt=""/>
                  <span className={'title'}>Wesele</span>
                </a>
                <a target="_blank" href={'https://photos.app.goo.gl/u8SdK1kf25WPWK457'} className={'image '}>
                  <img src={grupowe} alt=""/>
                  <span className={'title'}>Grupowe</span>
                </a>
                <a target="_blank" href={'https://photos.app.goo.gl/cjGvx3FXRMpXiu4d8'} className={'image '}>
                  <img src={plener} alt=""/>
                  <span className={'title'}>Plener</span>
                </a>
              </div>


              <div>
                <div className={'thanks'}><strong>Zapraszamy do filmików: </strong></div>
                <div className={'thanks'}>Filmik teledysk: </div>

                <a target="_blank" href={'https://www.youtube.com/watch?v=OZwaDBuXPCU'} className={'image '}>
                  <img src={movie} alt=""/>
                </a>
              </div>

              <div>
                <div className={'thanks '}>Filmik dłuższy: </div>
                <a target="_blank" href={'https://youtu.be/2QhD4byvMj4'} className={'image '}>
                  <img src={movie} alt=""/>
                </a>

              </div>

              <Footer></Footer>

            </div>
        );
    }
}

export default App;
