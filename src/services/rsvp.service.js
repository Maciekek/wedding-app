import { guestsRef } from "../firebase";

export const addToDo = (data) => {
    data.date = new Date().toISOString();
    console.log(data);
    return guestsRef.push().set(data);
};