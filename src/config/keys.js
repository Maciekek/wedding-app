if (process.env.NODE_ENV === "production") {
    module.exports = require("./prod/firebaseConfig");
} else {
    module.exports = require("./dev/firebaseConfig");
}

