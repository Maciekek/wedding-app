export const FirebaseConfig = {
    apiKey: "wedding-app-prod",
    authDomain: "<Project ID>.firebaseapp.com",
    databaseURL: "https://wedding-app-prod.firebaseio.com/"
};